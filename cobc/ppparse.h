/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_PP_PPPARSE_H_INCLUDED
# define YY_PP_PPPARSE_H_INCLUDED
/* Debug traces.  */
#ifndef PPDEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define PPDEBUG 1
#  else
#   define PPDEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define PPDEBUG 0
# endif /* ! defined YYDEBUG */
#endif  /* ! defined PPDEBUG */
#if PPDEBUG
extern int ppdebug;
#endif

/* Token type.  */
#ifndef PPTOKENTYPE
# define PPTOKENTYPE
  enum pptokentype
  {
    TOKEN_EOF = 0,
    ALSO = 258,
    BY = 259,
    COPY = 260,
    EQEQ = 261,
    IN = 262,
    LAST = 263,
    LEADING = 264,
    OF = 265,
    OFF = 266,
    PRINTING = 267,
    REPLACE = 268,
    REPLACING = 269,
    SUPPRESS = 270,
    TRAILING = 271,
    DOT = 272,
    GARBAGE = 273,
    LISTING_DIRECTIVE = 274,
    LISTING_STATEMENT = 275,
    TITLE_STATEMENT = 276,
    CONTROL_STATEMENT = 277,
    SOURCE = 278,
    NOSOURCE = 279,
    LIST = 280,
    NOLIST = 281,
    MAP = 282,
    NOMAP = 283,
    LEAP_SECOND_DIRECTIVE = 284,
    SOURCE_DIRECTIVE = 285,
    FORMAT = 286,
    IS = 287,
    FIXED = 288,
    FREE = 289,
    VARIABLE = 290,
    CALL_DIRECTIVE = 291,
    COBOL = 292,
    TOK_EXTERN = 293,
    STDCALL = 294,
    STATIC = 295,
    DEFINE_DIRECTIVE = 296,
    AS = 297,
    PARAMETER = 298,
    OVERRIDE = 299,
    SET_DIRECTIVE = 300,
    ADDRSV = 301,
    ADDSYN = 302,
    ASSIGN = 303,
    CALLFH = 304,
    XFD = 305,
    COMP1 = 306,
    CONSTANT = 307,
    FOLDCOPYNAME = 308,
    MAKESYN = 309,
    NOFOLDCOPYNAME = 310,
    REMOVE = 311,
    SOURCEFORMAT = 312,
    IF_DIRECTIVE = 313,
    ELSE_DIRECTIVE = 314,
    ENDIF_DIRECTIVE = 315,
    ELIF_DIRECTIVE = 316,
    GE = 317,
    LE = 318,
    LT = 319,
    GT = 320,
    EQ = 321,
    NE = 322,
    NOT = 323,
    THAN = 324,
    TO = 325,
    OR = 326,
    EQUAL = 327,
    GREATER = 328,
    LESS = 329,
    SET = 330,
    DEFINED = 331,
    TURN_DIRECTIVE = 332,
    ON = 333,
    CHECKING = 334,
    WITH = 335,
    LOCATION = 336,
    TERMINATOR = 337,
    TOKEN = 338,
    TEXT_NAME = 339,
    VARIABLE_NAME = 340,
    LITERAL = 341
  };
#endif
/* Tokens.  */
#define TOKEN_EOF 0
#define ALSO 258
#define BY 259
#define COPY 260
#define EQEQ 261
#define IN 262
#define LAST 263
#define LEADING 264
#define OF 265
#define OFF 266
#define PRINTING 267
#define REPLACE 268
#define REPLACING 269
#define SUPPRESS 270
#define TRAILING 271
#define DOT 272
#define GARBAGE 273
#define LISTING_DIRECTIVE 274
#define LISTING_STATEMENT 275
#define TITLE_STATEMENT 276
#define CONTROL_STATEMENT 277
#define SOURCE 278
#define NOSOURCE 279
#define LIST 280
#define NOLIST 281
#define MAP 282
#define NOMAP 283
#define LEAP_SECOND_DIRECTIVE 284
#define SOURCE_DIRECTIVE 285
#define FORMAT 286
#define IS 287
#define FIXED 288
#define FREE 289
#define VARIABLE 290
#define CALL_DIRECTIVE 291
#define COBOL 292
#define TOK_EXTERN 293
#define STDCALL 294
#define STATIC 295
#define DEFINE_DIRECTIVE 296
#define AS 297
#define PARAMETER 298
#define OVERRIDE 299
#define SET_DIRECTIVE 300
#define ADDRSV 301
#define ADDSYN 302
#define ASSIGN 303
#define CALLFH 304
#define XFD 305
#define COMP1 306
#define CONSTANT 307
#define FOLDCOPYNAME 308
#define MAKESYN 309
#define NOFOLDCOPYNAME 310
#define REMOVE 311
#define SOURCEFORMAT 312
#define IF_DIRECTIVE 313
#define ELSE_DIRECTIVE 314
#define ENDIF_DIRECTIVE 315
#define ELIF_DIRECTIVE 316
#define GE 317
#define LE 318
#define LT 319
#define GT 320
#define EQ 321
#define NE 322
#define NOT 323
#define THAN 324
#define TO 325
#define OR 326
#define EQUAL 327
#define GREATER 328
#define LESS 329
#define SET 330
#define DEFINED 331
#define TURN_DIRECTIVE 332
#define ON 333
#define CHECKING 334
#define WITH 335
#define LOCATION 336
#define TERMINATOR 337
#define TOKEN 338
#define TEXT_NAME 339
#define VARIABLE_NAME 340
#define LITERAL 341

/* Value type.  */
#if ! defined PPSTYPE && ! defined PPSTYPE_IS_DECLARED

union PPSTYPE
{
#line 518 "ppparse.y" /* yacc.c:1909  */

	char			*s;
	struct cb_text_list	*l;
	struct cb_replace_list	*r;
	struct cb_define_struct	*ds;
	unsigned int		ui;
	int			si;

#line 245 "ppparse.h" /* yacc.c:1909  */
};

typedef union PPSTYPE PPSTYPE;
# define PPSTYPE_IS_TRIVIAL 1
# define PPSTYPE_IS_DECLARED 1
#endif


extern PPSTYPE pplval;

int ppparse (void);

#endif /* !YY_PP_PPPARSE_H_INCLUDED  */
